import { NativeModules } from 'react-native';

type DeviceDetailsType = {
  id: string;
  name: string;
  phone: string;
};

export type DeviceType = {
  address: string;
  name: string;
};

export type OrderType = {
  invoiceNumber: string;
  totalAmount: number;
  orderItems: OrderItemType[];
};

export type OrderItemType = {
  id: string;
  isSku: boolean;
  name: string;
  description: string;
  quantity: number;
  price: number;
  taxable: boolean;
};

export type CreditCardType = {
  creditCardNumber: string;
  expirationMonth: string;
  expirationYear: string;
  cardCode: string;
};

export type PaymentType = {
  transactionId: string;
  amount: number;
};

export type PaymentResponseType = {
  accountNumber: string;
  accountType: string;
  authCode: string;
  authorizedAmount: string;
  avsResultCode?: {
    value: string;
    description: string;
  };
  cardCodeResponse?: {
    value: string;
    description: string;
  };
  cavvResultCode?: {
    value: string;
    description: string;
  };
  emvTLVResponse: string;
  emvTlvMap?: {
    [key: string]: string;
  };
  entryMode: string;
  isAuthorizeNet: boolean;
  isIssuerResponse: boolean;
  isShowSignature: boolean;
  isTestRequest: boolean;
  merchantDefinedMap?: {
    [key: string]: string;
  };
  prepaidCard?: {
    approvedAmount: number;
    balanceOnCard: number;
    requestedAmount: number;
  };
  refTransId: string;
  responseCode: {
    code: string;
    description: string;
  };
  responseText: string;
  signatureBase64: string;
  splitTenderId: string;
  splitTenderPayments?: {
    accountNumber: string;
    accountType: string;
    approvedAmount: number;
    authCode: string;
    balanceOnCard: number;
    requestedAmount: number;
    responseCode: {
      code: string;
      description: string;
    };
    responseToCustomer: string;
    transId: string;
  };
  tipAmount: string;
  transactionResponseErrors?: {
    notes: string;
    reasonText: string;
    responseCode: {
      code: string;
      description: string;
    };
    responseReasonCode: number;
  }[];
  transactionResponseMessages?: {
    notes: string;
    reasonText: string;
    responseCode: {
      code: string;
      description: string;
    };
    responseReasonCode: number;
  }[];
  transHash: string;
  transId: string;
};

export type MerchantDetailsType = {
  companyName: string;
  address: string;
  city: string;
  state: string;
  zip: string;
};

type AuthorizeNetInPersonType = {
  login(
    username: string,
    password: string,
    environment: AuthorizeNetEnvironment,
    device: DeviceDetailsType
  ): Promise<{
    name: string;
    device: string;
  }>;
  loggedIn(): Promise<boolean>;
  getMerchantDetails(): Promise<MerchantDetailsType>;
  getDevices(): Promise<DeviceType[]>;
  connectDevice(address: string): Promise<boolean>;
  reconnectDevice(address: string): Promise<boolean>;
  isDeviceConnected(): Promise<boolean>;
  makePayment(order: OrderType, signature: boolean): Promise<PaymentType>;
  makeManualPayment(
    order: OrderType,
    card: CreditCardType
  ): Promise<PaymentType>;
  hasCardData(): Promise<boolean>;
  checkUpdate(address: string): Promise<boolean>;
};

export enum AuthorizeNetEnvironment {
  SANDBOX = 'SANDBOX',
  SANDBOX_TESTMODE = 'SANDBOX_TESTMODE',
  PRODUCTION = 'PRODUCTION',
  PRODUCTION_TESTMODE = 'PRODUCTION_TESTMODE',
}

const { AuthorizeNetInPerson } = NativeModules;

export default AuthorizeNetInPerson as AuthorizeNetInPersonType;
