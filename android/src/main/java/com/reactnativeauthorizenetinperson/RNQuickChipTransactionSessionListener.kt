package com.reactnativeauthorizenetinperson

import android.bluetooth.BluetoothDevice
import com.facebook.react.bridge.Promise
import com.facebook.react.bridge.WritableNativeArray
import com.facebook.react.bridge.WritableNativeMap
import net.authorize.aim.emv.EMVDeviceConnectionType
import net.authorize.aim.emv.EMVErrorCode
import net.authorize.aim.emv.EMVTransactionManager


open class RNQuickChipTransactionSessionListener(
  private val promise: Promise,
  private val authorizeNetInPersonModule: AuthorizeNetInPersonModule,
  private val callback: (() -> Unit)? = null
): EMVTransactionManager.QuickChipTransactionSessionListener {
  override fun onReturnBluetoothDevices(bluetoothDeviceList: List<BluetoothDevice>) {
    authorizeNetInPersonModule.setBluetoothDevices(bluetoothDeviceList)
    var response = WritableNativeArray()

    for (device: BluetoothDevice in bluetoothDeviceList) {
      var entry = WritableNativeMap()
      entry.putString("address", device.address)
      entry.putString("name", device.name)
      response.pushMap(entry)
    }

    authorizeNetInPersonModule.sendMessage("Search Bluetooth Devices - Found " + bluetoothDeviceList.size + " devices")

    // check for audio device
    EMVTransactionManager.setDeviceConnectionType(EMVDeviceConnectionType.AUDIO)
    val status = EMVTransactionManager.getEmvReaderStatus()
    if (status == EMVTransactionManager.EMVReaderStatus.CONNECTION_INITIALIZED) {
      var entry = WritableNativeMap()
      entry.putString("address", "AUDIO")
      entry.putString("name", "Audio Input")
      response.pushMap(entry)
      authorizeNetInPersonModule.sendMessage("Audio device connected")
    }

    if (callback === null) promise.resolve(response)
    else {
      callback.invoke()
    }
  }

  override fun onBluetoothDeviceConnected(bluetoothDevice: BluetoothDevice) {
    authorizeNetInPersonModule.sendMessage("Bluetooth device connected : " + bluetoothDevice.name)
    promise.resolve(true)
  }

  override fun onBluetoothDeviceDisConnected() {
    authorizeNetInPersonModule.trigger("onDisconnecct", true)
    authorizeNetInPersonModule.sendMessage("Bluetooth device disconnected")
    promise.resolve(true)
  }

  override fun onTransactionStatusUpdate(transactionStatus: String) {
    authorizeNetInPersonModule.sendMessage("Transaction Status Update: $transactionStatus")
//    statusTextView.setTextColor(Color.GREEN)
//    statusTextView.setText(transactionStatus)
  }

  override fun onPrepareQuickChipDataSuccessful() {
    authorizeNetInPersonModule.sendMessage("Chip data saved Successfully")
    if (callback === null) promise.resolve(true)
    else {
      callback.invoke()
    }
//    statusTextView.setTextColor(Color.BLACK)
//    statusTextView.setText("Chip data saved Successfully")
  }

  override fun onPrepareQuickChipDataError(error: EMVErrorCode, cause: String) {
    authorizeNetInPersonModule.sendMessage("onPrepareQuickChipDataError")
    authorizeNetInPersonModule.sendMessage(cause)
    EMVTransactionManager.clearStoredQuickChipData(this)
    promise.reject("Prepare Quick Chip Error", cause)
//    statusTextView.setTextColor(Color.RED)
//    statusTextView.setText(cause)
  }

  override fun onEMVTransactionSuccessful(result: net.authorize.aim.emv.Result) {
    authorizeNetInPersonModule.sendMessage("onEMVTransactionSuccessful")

    val response = WritableNativeMap()
    response.putString("accountNumber", result.accountNumber)
    if (result.accountType != null) response.putString("accountType", result.accountType.value)
    response.putString("authCode", result.authCode)
    response.putString("authorizedAmount", result.authorizedAmount)

    if (result.avsResultCode != null) {
      val avsResultCode = WritableNativeMap()
      avsResultCode.putString("value", result.avsResultCode.value)
      avsResultCode.putString("description", result.avsResultCode.description)
      response.putMap("avsResultCode", avsResultCode)
    }

    if (result.cardCodeResponse != null) {
      val cardCodeResponse = WritableNativeMap()
      cardCodeResponse.putString("value", result.cardCodeResponse.value)
      cardCodeResponse.putString("description", result.cardCodeResponse.description)
      response.putMap("cardCodeResponse", cardCodeResponse)
    }

    if (result.cavvResultCode != null) {
      val cavvResultCode = WritableNativeMap()
      cavvResultCode.putString("value", result.cavvResultCode.value)
      cavvResultCode.putString("description", result.cavvResultCode.description)
      response.putMap("cavvResultCode", cavvResultCode)
    }

    response.putString("emvTLVResponse", result.emvTLVResponse)
    if (result.emvTlvMap != null) response.putMap("emvTlvMap", convertMap(result.emvTlvMap))
    response.putString("entryMode", result.entryMode)
    response.putBoolean("isAuthorizeNet", result.isAuthorizeNet)
    response.putBoolean("isIssuerResponse", result.isIssuerResponse)
    response.putBoolean("isShowSignature", result.isShowSignature)
    response.putBoolean("isTestRequest", result.isTestRequest)
    if (result.merchantDefinedMap != null) response.putMap("merchantDefinedMap", convertMap(result.merchantDefinedMap))

    if (result.prepaidCard != null) {
      val prepaidCard = WritableNativeMap()
      prepaidCard.putDouble("approvedAmount", result.prepaidCard.approvedAmount.longValueExact().toDouble())
      prepaidCard.putDouble("balanceOnCard", result.prepaidCard.balanceOnCard.longValueExact().toDouble())
      prepaidCard.putDouble("requestedAmount", result.prepaidCard.requestedAmount.longValueExact().toDouble())
      response.putMap("prepaidCard", prepaidCard)
    }

    response.putString("refTransId", result.refTransId)

    if (result.responseCode != null) {
      val responseCode = WritableNativeMap()
      responseCode.putInt("code", result.responseCode.code)
      responseCode.putString("description", result.responseCode.description)
      response.putMap("responseCode", responseCode)
    }

    response.putString("responseText", result.responseText)
    response.putString("signatureBase64", result.signatureBase64)
    response.putString("splitTenderId", result.splitTenderId)
    if (result.splitTenderPayments != null && result.splitTenderPayments.size > 0) {
      val splitTenderPayments = WritableNativeArray()
      for (splitTenderPayment in result.splitTenderPayments) {
        val splitTenderPaymentEntry = WritableNativeMap()
        splitTenderPaymentEntry.putString("accountNumber", splitTenderPayment.accountNumber)
        splitTenderPaymentEntry.putString("accountType", splitTenderPayment.accountType.value)
        splitTenderPaymentEntry.putDouble("approvedAmount", splitTenderPayment.approvedAmount.longValueExact().toDouble())
        splitTenderPaymentEntry.putString("authCode", splitTenderPayment.authCode)
        splitTenderPaymentEntry.putDouble("balanceOnCard", splitTenderPayment.balanceOnCard.longValueExact().toDouble())
        splitTenderPaymentEntry.putDouble("requestedAmount", splitTenderPayment.requestedAmount.longValueExact().toDouble())
        val splitTenderResponseCode = WritableNativeMap()
        splitTenderResponseCode.putInt("code", splitTenderPayment.responseCode.code)
        splitTenderResponseCode.putString("description", splitTenderPayment.responseCode.description)
        splitTenderPaymentEntry.putMap("responseCode", splitTenderResponseCode)
        splitTenderPaymentEntry.putString("responseToCustomer", splitTenderPayment.responseToCustomer)
        splitTenderPaymentEntry.putString("transId", splitTenderPayment.transId)
        splitTenderPayments.pushMap(splitTenderPaymentEntry)
      }
      response.putArray("splitTenderPayments", splitTenderPayments)
    }
    response.putString("tipAmount", result.tipAmount)
    if (result.transactionResponseErrors != null && result.transactionResponseErrors.size > 0) {
      val transactionResponseErrors = WritableNativeArray()
      for (transactionResponseError in result.transactionResponseErrors) {
        val transactionResponseErrorEntry = WritableNativeMap()
        transactionResponseErrorEntry.putString("notes", transactionResponseError.notes)
        transactionResponseErrorEntry.putString("reasonText", transactionResponseError.reasonText)
        val transactionResponseErrorResponseCode = WritableNativeMap()
        transactionResponseErrorResponseCode.putInt("code", transactionResponseError.responseCode.code)
        transactionResponseErrorResponseCode.putString("description", transactionResponseError.responseCode.description)
        transactionResponseErrorEntry.putMap("responseCode", transactionResponseErrorResponseCode)
        transactionResponseErrorEntry.putInt("responseReasonCode", transactionResponseError.responseReasonCode)
        transactionResponseErrors.pushMap(transactionResponseErrorEntry)
      }
      response.putArray("transactionResponseErrors", transactionResponseErrors)
    }
    if (result.transactionResponseMessages != null && result.transactionResponseMessages.size > 0) {
      val transactionResponseMessages = WritableNativeArray()
      for (transactionResponseMessage in result.transactionResponseMessages) {
        val transactionResponseMessageEntry = WritableNativeMap()
        transactionResponseMessageEntry.putString("notes", transactionResponseMessage.notes)
        transactionResponseMessageEntry.putString("reasonText", transactionResponseMessage.reasonText)
        val transactionResponseMessageResponseCode = WritableNativeMap()
        transactionResponseMessageResponseCode.putInt("code", transactionResponseMessage.responseCode.code)
        transactionResponseMessageResponseCode.putString("description", transactionResponseMessage.responseCode.description)
        transactionResponseMessageEntry.putMap("responseCode", transactionResponseMessageResponseCode)
        transactionResponseMessageEntry.putInt("responseReasonCode", transactionResponseMessage.responseReasonCode)
        transactionResponseMessages.pushMap(transactionResponseMessageEntry)
      }
      response.putArray("transactionResponseMessages", transactionResponseMessages)
    }
    response.putString("transHash", result.transHash)
    response.putString("transId", result.transId)
    promise.resolve(response)
  }

  private fun convertMap(map: Map<String, String>): WritableNativeMap {
    val result = WritableNativeMap()

    map.forEach {
      result.putString(it.key, it.value)
    }

    return result
  }

  override fun onEMVReadError(emvError: EMVErrorCode) {
    if (emvError != null) {
      authorizeNetInPersonModule.sendMessage("EMV Error: ${emvError.errorString}")
    } else authorizeNetInPersonModule.sendMessage("EMV Error")
    promise.reject("EMV Read Error", emvError.errorString)
  }

  override fun onEMVTransactionError(result: net.authorize.aim.emv.Result, emvError: EMVErrorCode) {
    authorizeNetInPersonModule.sendMessage("onEMVTransactionError")
    promise.reject("EMV Transaction Error", emvError.errorString)
//    processEmvTransactionError(result, emvError)
  }
}
