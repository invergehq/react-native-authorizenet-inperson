package com.reactnativeauthorizenetinperson

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.content.pm.PackageManager
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.facebook.react.bridge.*
import com.facebook.react.modules.core.DeviceEventManagerModule
import net.authorize.Environment
import net.authorize.Merchant
import net.authorize.aim.emv.EMVDeviceConnectionType
import net.authorize.aim.emv.EMVTransactionManager
import net.authorize.aim.emv.EMVTransactionType
import net.authorize.aim.emv.OTAUpdateManager
import net.authorize.auth.PasswordAuthentication
import net.authorize.auth.SessionTokenAuthentication
import net.authorize.data.Order
import net.authorize.data.OrderItem
import net.authorize.data.creditcard.CreditCard
import net.authorize.data.mobile.MobileDevice
import net.authorize.mobile.Result
import net.authorize.mobile.Transaction
import net.authorize.mobile.TransactionType
import net.authorize.xml.MessageType
import java.math.BigDecimal


class AuthorizeNetInPersonModule(reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext) {
  private var merchant: Merchant? = null
  private var bluetoothDevices: List<BluetoothDevice>? = null

  override fun getName(): String {
      return "AuthorizeNetInPerson"
  }

  fun setBluetoothDevices(bluetoothDeviceList: List<BluetoothDevice>) {
    bluetoothDevices = bluetoothDeviceList
  }

  fun sendMessage(message: String) {
    Log.d("RNAuthorizeNetInPerson", message)
    reactApplicationContext.getJSModule(
      DeviceEventManagerModule.RCTDeviceEventEmitter::class.java
    ).emit("onSendMessage", message)
  }

  fun sendProgress(percent: Double) {
    reactApplicationContext.getJSModule(
      DeviceEventManagerModule.RCTDeviceEventEmitter::class.java
    ).emit("onUpdateProgress", percent)
  }

  fun trigger(event: String, data: Any) {
    reactApplicationContext.getJSModule(
      DeviceEventManagerModule.RCTDeviceEventEmitter::class.java
    ).emit(event, data)
  }

  @ReactMethod
  fun addListener(eventName: String) {
    // Required so that ReactNative doesn't warn us about this function not existing.
  }

  @ReactMethod
  fun removeListeners(count: Int) {
    // Required so that ReactNative doesn't warn us about this function not existing.
  }

  @ReactMethod
  fun login(username: String, password: String, environment: String, device: ReadableMap, promise: Promise) {
    checkLocationEnabled(promise)
    var env = Environment.PRODUCTION

    if (environment == "SANDBOX") env = Environment.SANDBOX
    else if (environment == "SANDBOX_TESTMODE") env = Environment.SANDBOX_TESTMODE
    else if (environment == "PRODUCTION_TESTMODE") env = Environment.PRODUCTION_TESTMODE

    try {
      validateDevice(device)

      val deviceId = device.getString("id")
      val passAuth: PasswordAuthentication = PasswordAuthentication.createMerchantAuthentication(username, password, deviceId)
      val merchant = Merchant.createMerchant(env, passAuth)

      initializeMerchant(merchant, device, promise)
    } catch (ex: Exception) {
      promise.reject(ex)
    }
  }

  @ReactMethod
  fun loggedIn(promise: Promise) {
    promise.resolve(merchant != null)
  }

  @ReactMethod
  fun connectAudioDevice(promise: Promise) {
    try {
      EMVTransactionManager.setDeviceConnectionType(EMVDeviceConnectionType.AUDIO)
    } catch (ex: IllegalArgumentException) {
      promise.reject("Bluetooth", "Invalid bluetooth device")
    }
  }

  @ReactMethod
  fun getDevices(promise: Promise) {
    checkBluetoothEnabled(promise)
    checkLocationEnabled(promise)
    sendMessage("Search Bluetooth Devices...")
    val listener = RNQuickChipTransactionSessionListener(promise, this)
    EMVTransactionManager.setDeviceConnectionType(EMVDeviceConnectionType.BLUETOOTH)
    EMVTransactionManager.startBTScan(reactApplicationContext, listener)
  }

  @ReactMethod
  fun connectDevice(address: String, promise: Promise) {
    if (address === "AUDIO") {
      EMVTransactionManager.setDeviceConnectionType(EMVDeviceConnectionType.AUDIO)
      val status = EMVTransactionManager.getEmvReaderStatus()
      if (status == EMVTransactionManager.EMVReaderStatus.CONNECTION_INITIALIZED) promise.resolve(true)
      else promise.reject("Audio", "No device connected")
    } else {
      try {
        val adapter = BluetoothAdapter.getDefaultAdapter()
        val device = adapter.getRemoteDevice(address)

        val listener = RNQuickChipTransactionSessionListener(promise, this)
        EMVTransactionManager.setDeviceConnectionType(EMVDeviceConnectionType.BLUETOOTH)
        EMVTransactionManager.connectBTDevice(reactApplicationContext, device, listener)
      } catch (ex: IllegalArgumentException) {
        promise.reject("Bluetooth", "Invalid bluetooth device")
      }
    }
  }

  @ReactMethod
  fun reconnectDevice(address: String, promise: Promise) {
    if (address === "AUDIO") {
      EMVTransactionManager.setDeviceConnectionType(EMVDeviceConnectionType.AUDIO)
      val status = EMVTransactionManager.getEmvReaderStatus()
      if (status == EMVTransactionManager.EMVReaderStatus.CONNECTION_INITIALIZED) promise.resolve(true)
      else promise.reject("Audio", "No device connected")
    } else {
      try {
        val listener = RNQuickChipTransactionSessionListener(promise, this)
        EMVTransactionManager.setDeviceConnectionType(EMVDeviceConnectionType.BLUETOOTH)
        EMVTransactionManager.connectBTDevice(reactApplicationContext, null, listener)
      } catch (ex: IllegalArgumentException) {
        promise.reject("Bluetooth", "Invalid bluetooth device")
      }
    }
  }

  @ReactMethod
  fun isDeviceConnected(promise: Promise) {
    val status = EMVTransactionManager.getEmvReaderStatus()

    promise.resolve(status == EMVTransactionManager.EMVReaderStatus.CONNECTION_INITIALIZED)
  }

  @ReactMethod
  fun getBluetoothDevices(promise: Promise) {
    checkBluetoothEnabled(promise)
    checkLocationEnabled(promise)
    sendMessage("Search Bluetooth Devices...")
    val listener = RNQuickChipTransactionSessionListener(promise, this)
    EMVTransactionManager.setDeviceConnectionType(EMVDeviceConnectionType.BLUETOOTH)
    EMVTransactionManager.startBTScan(reactApplicationContext, listener)
  }

  @ReactMethod
  fun connectBluetoothDevice(address: String, promise: Promise) {
    try {
      val adapter = BluetoothAdapter.getDefaultAdapter()
      val device = adapter.getRemoteDevice(address)

      val listener = RNQuickChipTransactionSessionListener(promise, this)
      EMVTransactionManager.setDeviceConnectionType(EMVDeviceConnectionType.BLUETOOTH)
      EMVTransactionManager.connectBTDevice(reactApplicationContext, device, listener)
    } catch (ex: IllegalArgumentException) {
      promise.reject("Bluetooth", "Invalid bluetooth device")
    }
  }

  @ReactMethod
  fun reconnectBluetoothDevice(promise: Promise) {
    try {
      val listener = RNQuickChipTransactionSessionListener(promise, this)
      EMVTransactionManager.setDeviceConnectionType(EMVDeviceConnectionType.BLUETOOTH)
      EMVTransactionManager.connectBTDevice(reactApplicationContext, null, listener)
    } catch (ex: IllegalArgumentException) {
      promise.reject("Bluetooth", "Invalid bluetooth device")
    }
  }

  @ReactMethod
  fun isBluetoothDeviceConnected(promise: Promise) {
    val status = EMVTransactionManager.getEmvReaderStatus()

    promise.resolve(status == EMVTransactionManager.EMVReaderStatus.CONNECTION_INITIALIZED)
  }

  @ReactMethod
  fun makePayment(order: ReadableMap, signature: Boolean, promise: Promise) {
    val o = loadOrder(order)
    val emvTransaction = EMVTransactionManager.createEMVTransaction(merchant, o.totalAmount)
    emvTransaction.emvTransactionType = EMVTransactionType.GOODS
    emvTransaction.order = o

//    val listener = RNQuickChipTransactionSessionListener(promise, this) {
//      val transactionListener = RNQuickChipTransactionSessionListener(promise, this)
//      EMVTransactionManager.startQuickChipTransaction(emvTransaction, transactionListener, reactApplicationContext.currentActivity)
//    }
//    EMVTransactionManager.prepareDataForQuickChipTransaction(reactApplicationContext.currentActivity, listener)

    val listener = RNQuickChipTransactionSessionListener(promise, this)
    EMVTransactionManager.startQuickChipTransaction(emvTransaction, listener, reactApplicationContext.currentActivity, signature)
  }

  @ReactMethod
  fun getMerchantDetails(promise: Promise) {
    val transaction = Transaction.createTransaction(merchant, TransactionType.GET_MERCHANT_DETAILS)

    val result = merchant?.postTransaction(transaction) as Result

    promise.resolve(result.xmlResponse)
  }

  @ReactMethod
  fun checkUpdate(address: String, promise: Promise) {
    try {
      val adapter = BluetoothAdapter.getDefaultAdapter()
      val device = adapter.getRemoteDevice(address)

      sendMessage("Connect device")
      RNHeadlessOTAUpdateListener(promise, this) {
        sendMessage("Callback")
        val listener = RNHeadlessOTAUpdateListener(promise, this)
        OTAUpdateManager.checkForOTAUpdates(reactApplicationContext, false, listener)
      }
      OTAUpdateManager.setDeviceConnectionType(EMVDeviceConnectionType.BLUETOOTH)
      OTAUpdateManager.connectBluetoothDevice(device)
    } catch (ex: IllegalArgumentException) {
      promise.reject("Bluetooth", "Invalid bluetooth device")
    }
  }

  @ReactMethod
  fun startUpdate(promise: Promise) {
    RNHeadlessOTAUpdateListener(promise, this)
    OTAUpdateManager.startOTAUpdate(reactApplicationContext, false)
  }

  @ReactMethod
  fun makeManualPayment(order: ReadableMap, card: ReadableMap, promise: Promise) {
    val o = loadOrder(order)
    val creditCard = CreditCard.createCreditCard()
    creditCard.creditCardNumber = if (card.hasKey("creditCardNumber")) card.getString("creditCardNumber") else ""
    creditCard.expirationMonth = if (card.hasKey("expirationMonth")) card.getString("expirationMonth") else ""
    creditCard.expirationYear = if (card.hasKey("expirationYear")) card.getString("expirationYear") else ""
    creditCard.cardCode = if (card.hasKey("cardCode")) card.getString("cardCode") else ""

    val transaction = net.authorize.aim.Transaction.createTransaction(merchant, net.authorize.TransactionType.AUTH_CAPTURE, o.totalAmount)
    transaction.order = o
    transaction.creditCard = creditCard

    val result = merchant?.postTransaction(transaction) as net.authorize.aim.Result

    if (result.isOk) {
      val response = WritableNativeMap()
      response.putString("accountNumber", result.accountNumber)
      if (result.accountType != null) response.putString("accountType", result.accountType.value)
      response.putString("authCode", result.authCode)

      if (result.avsResultCode != null) {
        val avsResultCode = WritableNativeMap()
        avsResultCode.putString("value", result.avsResultCode.value)
        avsResultCode.putString("description", result.avsResultCode.description)
        response.putMap("avsResultCode", avsResultCode)
      }

      if (result.cardCodeResponse != null) {
        val cardCodeResponse = WritableNativeMap()
        cardCodeResponse.putString("value", result.cardCodeResponse.value)
        cardCodeResponse.putString("description", result.cardCodeResponse.description)
        response.putMap("cardCodeResponse", cardCodeResponse)
      }

      if (result.cavvResultCode != null) {
        val cavvResultCode = WritableNativeMap()
        cavvResultCode.putString("value", result.cavvResultCode.value)
        cavvResultCode.putString("description", result.cavvResultCode.description)
        response.putMap("cavvResultCode", cavvResultCode)
      }

      response.putBoolean("isAuthorizeNet", result.isAuthorizeNet)
      response.putBoolean("isTestRequest", result.isTestRequest)
      if (result.merchantDefinedMap != null) response.putMap("merchantDefinedMap", convertMap(result.merchantDefinedMap))

      if (result.prepaidCard != null) {
        val prepaidCard = WritableNativeMap()
        prepaidCard.putDouble("approvedAmount", result.prepaidCard.approvedAmount.longValueExact().toDouble())
        prepaidCard.putDouble("balanceOnCard", result.prepaidCard.balanceOnCard.longValueExact().toDouble())
        prepaidCard.putDouble("requestedAmount", result.prepaidCard.requestedAmount.longValueExact().toDouble())
        response.putMap("prepaidCard", prepaidCard)
      }

      response.putString("refTransId", result.refTransId)

      if (result.responseCode != null) {
        val responseCode = WritableNativeMap()
        responseCode.putInt("code", result.responseCode.code)
        responseCode.putString("description", result.responseCode.description)
        response.putMap("responseCode", responseCode)
      }

      response.putString("responseText", result.responseText)
      response.putString("splitTenderId", result.splitTenderId)
      if (result.splitTenderPayments != null && result.splitTenderPayments.size > 0) {
        val splitTenderPayments = WritableNativeArray()
        for (splitTenderPayment in result.splitTenderPayments) {
          val splitTenderPaymentEntry = WritableNativeMap()
          splitTenderPaymentEntry.putString("accountNumber", splitTenderPayment.accountNumber)
          splitTenderPaymentEntry.putString("accountType", splitTenderPayment.accountType.value)
          splitTenderPaymentEntry.putDouble("approvedAmount", splitTenderPayment.approvedAmount.longValueExact().toDouble())
          splitTenderPaymentEntry.putString("authCode", splitTenderPayment.authCode)
          splitTenderPaymentEntry.putDouble("balanceOnCard", splitTenderPayment.balanceOnCard.longValueExact().toDouble())
          splitTenderPaymentEntry.putDouble("requestedAmount", splitTenderPayment.requestedAmount.longValueExact().toDouble())
          val splitTenderResponseCode = WritableNativeMap()
          splitTenderResponseCode.putInt("code", splitTenderPayment.responseCode.code)
          splitTenderResponseCode.putString("description", splitTenderPayment.responseCode.description)
          splitTenderPaymentEntry.putMap("responseCode", splitTenderResponseCode)
          splitTenderPaymentEntry.putString("responseToCustomer", splitTenderPayment.responseToCustomer)
          splitTenderPaymentEntry.putString("transId", splitTenderPayment.transId)
          splitTenderPayments.pushMap(splitTenderPaymentEntry)
        }
        response.putArray("splitTenderPayments", splitTenderPayments)
      }
      if (result.transactionResponseErrors != null && result.transactionResponseErrors.size > 0) {
        val transactionResponseErrors = WritableNativeArray()
        for (transactionResponseError in result.transactionResponseErrors) {
          val transactionResponseErrorEntry = WritableNativeMap()
          transactionResponseErrorEntry.putString("notes", transactionResponseError.notes)
          transactionResponseErrorEntry.putString("reasonText", transactionResponseError.reasonText)
          val transactionResponseErrorResponseCode = WritableNativeMap()
          transactionResponseErrorResponseCode.putInt("code", transactionResponseError.responseCode.code)
          transactionResponseErrorResponseCode.putString("description", transactionResponseError.responseCode.description)
          transactionResponseErrorEntry.putMap("responseCode", transactionResponseErrorResponseCode)
          transactionResponseErrorEntry.putInt("responseReasonCode", transactionResponseError.responseReasonCode)
          transactionResponseErrors.pushMap(transactionResponseErrorEntry)
        }
        response.putArray("transactionResponseErrors", transactionResponseErrors)
      }
      if (result.transactionResponseMessages != null && result.transactionResponseMessages.size > 0) {
        val transactionResponseMessages = WritableNativeArray()
        for (transactionResponseMessage in result.transactionResponseMessages) {
          val transactionResponseMessageEntry = WritableNativeMap()
          transactionResponseMessageEntry.putString("notes", transactionResponseMessage.notes)
          transactionResponseMessageEntry.putString("reasonText", transactionResponseMessage.reasonText)
          val transactionResponseMessageResponseCode = WritableNativeMap()
          transactionResponseMessageResponseCode.putInt("code", transactionResponseMessage.responseCode.code)
          transactionResponseMessageResponseCode.putString("description", transactionResponseMessage.responseCode.description)
          transactionResponseMessageEntry.putMap("responseCode", transactionResponseMessageResponseCode)
          transactionResponseMessageEntry.putInt("responseReasonCode", transactionResponseMessage.responseReasonCode)
          transactionResponseMessages.pushMap(transactionResponseMessageEntry)
        }
        response.putArray("transactionResponseMessages", transactionResponseMessages)
      }
      response.putString("transHash", result.transHash)
      response.putString("transId", result.transId)
      promise.resolve(response)
    } else {
      promise.reject("Error", result.responseText)
    }
  }

  private fun loadOrder(order: ReadableMap): Order {
    val o = Order.createOrder()
    o.invoiceNumber = if (order.hasKey("invoiceNumber")) order.getString("invoiceNumber") else ""
    o.totalAmount = if (order.hasKey("totalAmount")) BigDecimal(order.getDouble("totalAmount")) else BigDecimal(0)
    var price = BigDecimal(0)

    if (order.hasKey("orderItems")) {
      val orderItems = order.getArray("orderItems")

      for (i in 0 until (orderItems?.size() ?: 0)) {
        val orderItem = orderItems?.getMap(i)

        val oi = OrderItem.createOrderItem()
        oi.itemId = if (orderItem?.hasKey("id") == true) orderItem.getString("id") else ""
        oi.isSKU = if (orderItem?.hasKey("sku") == true) orderItem.getBoolean("sku") else false
        oi.itemName = if (orderItem?.hasKey("name") == true) orderItem.getString("name") else ""
        oi.itemDescription = if (orderItem?.hasKey("description") == true) orderItem.getString("description") else ""
        oi.itemQuantity = if (orderItem?.hasKey("quantity") == true) BigDecimal(orderItem.getInt("quantity")) else BigDecimal(0)
        oi.itemPrice = if (orderItem?.hasKey("price") == true) BigDecimal(orderItem.getDouble("price")) else BigDecimal(0)
        oi.isItemTaxable = if (orderItem?.hasKey("taxable") == true) orderItem.getBoolean("taxable") else false

        o.addOrderItem(oi)
        price = price.plus(oi.itemPrice)
      }
    }

    return o
  }

  private fun convertMap(map: Map<String, String>): WritableNativeMap {
    val result = WritableNativeMap()

    map.forEach {
      result.putString(it.key, it.value)
    }

    return result
  }

  private fun checkLocationEnabled(promise: Promise) {
    if ( ContextCompat.checkSelfPermission(reactApplicationContext, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
      val activity = reactApplicationContext.currentActivity
      if(activity != null) {
        ActivityCompat.requestPermissions(activity, Array(1) { android.Manifest.permission.ACCESS_FINE_LOCATION },
          1)
      }
    }
  }

  private fun checkBluetoothEnabled(promise: Promise) {
    val blueToothAdapter = BluetoothAdapter.getDefaultAdapter()

    if (blueToothAdapter === null) promise.reject("Bluetooth", "There is no bluetooth on this device")
    else {
      if (!blueToothAdapter.isEnabled) {
        val enableBluetoothIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
        val activity = reactApplicationContext.currentActivity
        if(activity != null) {
          ActivityCompat.startActivityForResult(activity, enableBluetoothIntent, 1, null)
        }
        sendMessage("Bluetooth Must Be Enabled")
      }
    }
  }

  private fun initializeMerchant(merchant: Merchant, device: ReadableMap, promise: Promise) {
    try {
      validateDevice(device)

      val deviceId = device.getString("id")
      val deviceName = device.getString("name")
      val devicePhone = device.getString("phone")

      val transaction: Transaction = merchant.createMobileTransaction(TransactionType.MOBILE_DEVICE_LOGIN)
      val mobileDevice = MobileDevice.createMobileDevice(deviceId, deviceName, devicePhone, "Android")

      transaction.mobileDevice = mobileDevice

      val result = merchant.postTransaction(transaction) as Result

      if (result.isOk) {
        try {
          val sessionTokenAuthentication = SessionTokenAuthentication.createMerchantAuthentication(
            merchant.merchantAuthentication.name,
            result.sessionToken,
            deviceId
          )

          if (
            result.sessionToken != null
            && sessionTokenAuthentication != null
          ) {
            merchant.merchantAuthentication = sessionTokenAuthentication
            this.merchant = merchant

            val response = WritableNativeMap()
            response.putString("name", result.merchantContact.companyName)
            response.putString("device", result.merchantAccountDetails.deviceType.value)
            promise.resolve(response)
          } else {
            for (message: MessageType in result.messages) {
              promise.reject("login", message.text)
              break
            }
          }
        } catch (ex: Exception) {
          promise.reject(ex)
        }
      } else {
        for (message: MessageType in result.messages) {
          promise.reject("login", message.text)
          break
        }
      }
    } catch (ex: Exception) {
      promise.reject(ex)
    }
  }

  private fun validateDevice(device: ReadableMap) {
    if (!device.hasKey("id")) throw Exception("device id required")
    if (!device.hasKey("name")) throw Exception("device name required")
  }
}
