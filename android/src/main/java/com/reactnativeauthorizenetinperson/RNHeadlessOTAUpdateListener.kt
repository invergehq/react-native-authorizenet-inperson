package com.reactnativeauthorizenetinperson

import android.bluetooth.BluetoothDevice
import com.bbpos.bbdevice.BBDeviceController
import com.facebook.react.bridge.Promise
import com.facebook.react.bridge.WritableNativeMap
import net.authorize.aim.emv.OTAUpdateManager
import java.util.*


open class RNHeadlessOTAUpdateListener(
  private val promise: Promise,
  private val authorizeNetInPersonModule: AuthorizeNetInPersonModule,
  private val callback: (() -> Unit)? = null
): OTAUpdateManager.HeadlessOTAUpdateListener {
  override fun onReturnOTAUpdateHeadlessProgress(var1: OTAUpdateManager.HeadlessOTAUpdateStatus?, var2: Double) {
    authorizeNetInPersonModule.sendProgress(var2)
  }

  override fun onReturnCheckForUpdateResult(var1: OTAUpdateManager.HeadlessOTACheckResult?) {
    promise.resolve(var1?.needUpdate())
  }

  override fun onReturnOTAUpdateHeadlessResult(var1: OTAUpdateManager.HeadlessOTAUpdateType?, var2: OTAUpdateManager.HeadlessOTAUpdateResult?, var3: String?) {
    promise.resolve(var3)
  }

  override fun onReturnDeviceInfo(var1: Hashtable<String?, String?>?) {
    val response = WritableNativeMap()

    for (i in var1?.asIterable()!!) {
      i.key?.let { response.putString(it, i.value) }
    }

    promise.resolve(response)
  }

  override fun onReturnOTAUpdateError(var1: OTAUpdateManager.HeadlessOTAUpdateError?, var2: String?) {
    promise.reject("Update", var2)
  }

  override fun onBluetoothScanTimeout() {
    authorizeNetInPersonModule.sendMessage("Bluetooth scan timeout")
  }

  override fun onReturnBluetoothDevices(var1: List<BluetoothDevice?>?) {
    authorizeNetInPersonModule.sendMessage("Bluetooth devices listed")
  }

  override fun onBluetoothDeviceConnected(var1: BluetoothDevice?) {
    authorizeNetInPersonModule.sendMessage("Bluetooth connected")
    if (callback != null) callback.invoke()
    else promise.resolve(true)
  }

  override fun onBluetoothDeviceDisConnected() {
    authorizeNetInPersonModule.sendMessage("Bluetooth disconnected")
  }

  override fun onAudioAutoConfigProgressUpdate(var1: Double) {

  }

  override fun onAudioAutoConfigCompleted(var1: Boolean, var2: String?) {

  }

  override fun onAudioAutoConfigError(var1: BBDeviceController.AudioAutoConfigError?) {

  }
}
