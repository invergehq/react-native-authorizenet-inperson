# react-native-authorizenet-inperson

React native module implementing Authorize.net's In-Person SDK

## Installation

```sh
npm install react-native-authorizenet-inperson
```

## Usage

```js
import AuthorizenetInperson from "react-native-authorizenet-inperson";

// ...

const result = await AuthorizenetInperson.multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
