import * as React from 'react';

import {
  NativeEventEmitter,
  NativeModules,
  StyleSheet,
  View,
} from 'react-native';
import AuthorizeNetInPerson from 'react-native-authorizenet-inperson';
import Login from './Containers/Login';
import Connect from './Containers/Connect';
import Payment from './Containers/Payment';
import ManualPayment from './Containers/ManualPayment';
import Button from './Components/Button';

const nativeEvent = new NativeEventEmitter(NativeModules.AuthorizeNetInPerson);
nativeEvent.addListener('onSendMessage', (message) => {
  console.log(message);
});

export default function App() {
  const [loggedIn, setLoggedIn] = React.useState<boolean>(false);
  const [connected, setConnected] = React.useState<boolean>(false);
  const [address, setAddress] = React.useState<string | undefined>();

  const setConnection = (_address: string) => {
    setAddress(_address);
    setConnected(true);
  };

  const getMerchantDetails = () => {
    doGetMerchantDetails();
  };

  const doGetMerchantDetails = async () => {
    const details = await AuthorizeNetInPerson.getMerchantDetails();

    console.log(details);
  };

  const checkUpdate = () => {
    doCheckUpdate();
  };

  const doCheckUpdate = async () => {
    if (address) {
      const details = await AuthorizeNetInPerson.checkUpdate(address);

      console.log(details);
    }
  };

  return (
    <>
      {loggedIn ? (
        <View style={styles.container}>
          {connected ? (
            <>
              <View style={styles.payments}>
                <Payment address={address || ''} connected={setConnected} />
                <ManualPayment />
              </View>
              <Button
                title="Check Update"
                style="color-primary-500"
                onPress={checkUpdate}
              />
            </>
          ) : (
            <Connect connected={setConnection} />
          )}
          <Button
            title="Get Details"
            style="color-info-500"
            onPress={getMerchantDetails}
          />
        </View>
      ) : (
        <Login login={setLoggedIn} />
      )}
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    flex: 1,
  },
  payments: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
  },
});
