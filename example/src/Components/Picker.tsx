import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import colors from '../Styles/theme.json';

interface CustomPickerProps {
  label: string;
  value: string;
  options: {
    label: string;
    value: string;
  }[];
  update: ((value: string) => void) | undefined;
}

export default function CustomPicker({
  value,
  options,
  update,
  ...props
}: CustomPickerProps) {
  return (
    <View style={styles.field}>
      <Picker {...props} onValueChange={update} selectedValue={value}>
        {options.map((option, index) => (
          <Picker.Item key={index} label={option.label} value={option.value} />
        ))}
      </Picker>
    </View>
  );
}

const styles = StyleSheet.create({
  field: {
    margin: 10,
    borderWidth: 1,
    borderColor: colors['color-light-500'],
    borderRadius: 2,
  },
});
