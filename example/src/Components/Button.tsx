import React from 'react';
import { ButtonProps, Button } from 'react-native';
import colors from '../Styles/theme.json';

interface CustomButtonProps extends ButtonProps {
  style: keyof typeof colors;
}

export default function CustomButton({ style, ...props }: CustomButtonProps) {
  const color = colors[style] || colors['color-primary-500'];

  return <Button {...props} color={color} />;
}
