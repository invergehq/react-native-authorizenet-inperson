import React from 'react';
import { StyleSheet, TextInput, TextInputProps, View } from 'react-native';
import colors from '../Styles/theme.json';

interface CustomTextInputProps extends TextInputProps {
  value: string;
  update: ((text: string) => void) | undefined;
}

export default function CustomTextInput({
  value,
  update,
  ...props
}: CustomTextInputProps) {
  return (
    <View style={styles.field}>
      <TextInput
        {...props}
        style={styles.input}
        value={value}
        onChangeText={update}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  field: {
    margin: 10,
  },
  input: {
    padding: 10,
    borderWidth: 1,
    borderColor: colors['color-light-500'],
    borderRadius: 2,
  },
});
