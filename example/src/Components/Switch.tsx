import React from 'react';
import { StyleSheet, Switch, SwitchProps, Text, View } from 'react-native';
import colors from '../Styles/theme.json';

interface CustomSwitchProps extends SwitchProps {
  label: string;
  value: boolean;
  update: ((value: boolean) => void) | undefined;
}

export default function CustomSwitch({
  label,
  value,
  update,
  ...props
}: CustomSwitchProps) {
  return (
    <View style={styles.field}>
      <Text>{label}</Text>
      <Switch
        {...props}
        trackColor={{
          false: colors['color-light-500'],
          true: colors['color-primary-500'],
        }}
        thumbColor={
          value ? colors['color-primary-500'] : colors['color-secondary-500']
        }
        ios_backgroundColor="#3e3e3e"
        onValueChange={update}
        value={value}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  field: {
    margin: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
});
