import * as React from 'react';

import {
  StyleSheet,
  View,
  Text,
  NativeSyntheticEvent,
  NativeTouchEvent,
  ActivityIndicator,
} from 'react-native';
import AuthorizeNetInPerson, {
  DeviceType,
} from 'react-native-authorizenet-inperson';
import colors from '../Styles/theme.json';
import Button from '../Components/Button';

interface ConnectProps {
  connected: (connected: string) => void;
}

export default function Connect({ connected }: ConnectProps) {
  const [loading, setLoading] = React.useState<boolean>(false);
  const [devices, setDevices] = React.useState<DeviceType[] | undefined>();
  const [error, setError] = React.useState<string | undefined>();

  const searchBluetooth = (_ev: NativeSyntheticEvent<NativeTouchEvent>) => {
    setLoading(true);
    setError(undefined);
    doSearchBluetooth();
  };

  const doSearchBluetooth = async () => {
    try {
      const result = await AuthorizeNetInPerson.getDevices();

      setLoading(false);
      setDevices(result);
    } catch (_error) {
      setError(_error.message);
      setLoading(false);
    }
  };

  const connect = async (address: string) => {
    setLoading(true);
    try {
      await AuthorizeNetInPerson.connectDevice(address);
      connected(address);
    } catch (_error) {
      setError(_error.message);
      setLoading(false);
    }
  };

  return (
    <View style={styles.container}>
      {loading ? (
        <ActivityIndicator size="large" color={colors['color-info-500']} />
      ) : (
        <View style={styles.box}>
          <Button
            title="Search for devices"
            style="color-success-500"
            onPress={searchBluetooth}
          />
          {error && <Text style={styles.error}>{error}</Text>}
          {devices && (
            <View style={styles.devices}>
              <Text style={styles.heading}>Connect to</Text>
              {devices.map((device) => (
                <View key={device.address} style={styles.device}>
                  <Button
                    title={device.name}
                    style="color-info-500"
                    onPress={() => connect(device.address)}
                  />
                </View>
              ))}
            </View>
          )}
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: 240,
  },
  heading: {
    fontSize: 24,
    textAlign: 'center',
  },
  devices: {
    marginVertical: 10,
  },
  device: {
    marginVertical: 10,
  },
  error: {
    color: colors['color-danger-500'],
    padding: 10,
    textAlign: 'center',
  },
});
