import * as React from 'react';

import {
  StyleSheet,
  View,
  Text,
  NativeSyntheticEvent,
  NativeTouchEvent,
  ActivityIndicator,
} from 'react-native';
import AuthorizeNetInPerson, {
  OrderType,
} from 'react-native-authorizenet-inperson';
import colors from '../Styles/theme.json';
import Button from '../Components/Button';
import Switch from '../Components/Switch';
import TextInput from '../Components/TextInput';

interface PaymentProps {
  address: string;
  connected: (connected: boolean) => void;
}

export default function Payment({ address, connected }: PaymentProps) {
  const [loading, setLoading] = React.useState<boolean>(false);
  const [amount, setAmount] = React.useState<string>('1.00');
  const [signature, setSignature] = React.useState<boolean>(false);
  const [error, setError] = React.useState<string | undefined>();

  const makePayment = (_ev: NativeSyntheticEvent<NativeTouchEvent>) => {
    const price = parseFloat(amount);

    if (price > 0) {
      setLoading(true);
      setError(undefined);
      doMakePayment(price);
    } else setError('The amount must be greater than zero');
  };

  const doMakePayment = async (price: number) => {
    const connection = await AuthorizeNetInPerson.isDeviceConnected();

    if (!connection) await AuthorizeNetInPerson.reconnectDevice(address);

    try {
      const order: OrderType = {
        invoiceNumber: '1',
        totalAmount: price,
        orderItems: [],
      };

      order.orderItems.push({
        id: '1',
        isSku: true,
        name: 'Test item',
        description: 'This is a test item',
        quantity: 1,
        price: price,
        taxable: false,
      });

      const result = await AuthorizeNetInPerson.makePayment(order, signature);

      console.log(result);
      setLoading(false);
    } catch (_error) {
      if (!(await AuthorizeNetInPerson.isDeviceConnected()))
        await AuthorizeNetInPerson.reconnectDevice(address);
      setError(_error.message);
      setLoading(false);
    }
  };

  const testConnection = async () => {
    const result = await AuthorizeNetInPerson.isDeviceConnected();

    connected(result);
  };

  return (
    <View style={styles.container}>
      <View style={styles.box}>
        <TextInput
          value={amount}
          update={setAmount}
          keyboardType="decimal-pad"
        />
        <Switch label="Signature" update={setSignature} value={signature} />
        <Button
          title="Make payment"
          style="color-success-500"
          onPress={makePayment}
        />
        <Button
          title="Test connection"
          style="color-info-500"
          onPress={testConnection}
        />
        {loading && (
          <ActivityIndicator size="large" color={colors['color-info-500']} />
        )}
        {error && <Text style={styles.error}>{error}</Text>}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: 240,
  },
  heading: {
    fontSize: 24,
    textAlign: 'center',
  },
  devices: {
    marginVertical: 10,
  },
  device: {
    marginVertical: 10,
  },
  error: {
    color: colors['color-danger-500'],
    padding: 10,
    textAlign: 'center',
  },
});
