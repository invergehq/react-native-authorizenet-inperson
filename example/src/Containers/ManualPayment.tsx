import * as React from 'react';

import {
  StyleSheet,
  View,
  Text,
  NativeSyntheticEvent,
  NativeTouchEvent,
  ActivityIndicator,
} from 'react-native';
import AuthorizeNetInPerson, {
  OrderType,
} from 'react-native-authorizenet-inperson';
import colors from '../Styles/theme.json';
import Button from '../Components/Button';

export default function ManualPayment() {
  const [loading, setLoading] = React.useState<boolean>(false);
  const [error, setError] = React.useState<string | undefined>();

  const makeManualPayment = (_ev: NativeSyntheticEvent<NativeTouchEvent>) => {
    const price = 1.0;

    if (price > 0) {
      setLoading(true);
      setError(undefined);
      doMakeManualPayment(price);
    } else setError('The amount must be greater than zero');
  };

  const doMakeManualPayment = async (price: number) => {
    try {
      const order: OrderType = {
        invoiceNumber: '1',
        totalAmount: price,
        orderItems: [],
      };

      order.orderItems.push({
        id: '1',
        isSku: true,
        name: 'Test item',
        description: 'This is a test item',
        quantity: 1,
        price: price,
        taxable: false,
      });

      const creditCard = {
        creditCardNumber: '4111111111111111',
        expirationMonth: '11',
        expirationYear: '2022',
        cardCode: '123',
      };

      const result = await AuthorizeNetInPerson.makeManualPayment(
        order,
        creditCard
      );

      console.log(result);
      setLoading(false);
    } catch (_error) {
      setError(_error.message);
      setLoading(false);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.box}>
        <Button
          title="Make ManualPayment"
          style="color-success-500"
          onPress={makeManualPayment}
        />
        {loading && (
          <ActivityIndicator size="large" color={colors['color-info-500']} />
        )}
        {error && <Text style={styles.error}>{error}</Text>}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: 240,
  },
  heading: {
    fontSize: 24,
    textAlign: 'center',
  },
  devices: {
    marginVertical: 10,
  },
  device: {
    marginVertical: 10,
  },
  error: {
    color: colors['color-danger-500'],
    padding: 10,
    textAlign: 'center',
  },
});
