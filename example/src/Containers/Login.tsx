import * as React from 'react';

import {
  StyleSheet,
  View,
  Text,
  NativeSyntheticEvent,
  NativeTouchEvent,
  ActivityIndicator,
} from 'react-native';
import AuthorizeNetInPerson, {
  AuthorizeNetEnvironment,
} from 'react-native-authorizenet-inperson';
import colors from '../Styles/theme.json';
import Button from '../Components/Button';
import Switch from '../Components/Switch';
import TextInput from '../Components/TextInput';

interface LoginProps {
  login: (loggedIn: boolean) => void;
}
export default function Login({ login }: LoginProps) {
  const [loading, setLoading] = React.useState<boolean>(false);
  const [username, setUsername] = React.useState<string>('');
  const [password, setPassword] = React.useState<string>('');
  const [sandbox, setSandbox] = React.useState<boolean>(false);
  const [test, setTest] = React.useState<boolean>(false);
  const [error, setError] = React.useState<string | undefined>();

  const attemptLogin = (_ev: NativeSyntheticEvent<NativeTouchEvent>) => {
    setLoading(true);
    setError(undefined);
    doLogin();
  };

  const doLogin = async () => {
    let environment = AuthorizeNetEnvironment.SANDBOX;
    if (sandbox && test) environment = AuthorizeNetEnvironment.SANDBOX_TESTMODE;
    else if (!sandbox && test)
      environment = AuthorizeNetEnvironment.PRODUCTION_TESTMODE;
    else if (!sandbox) environment = AuthorizeNetEnvironment.PRODUCTION;

    try {
      let result;

      result = await AuthorizeNetInPerson.login(
        username,
        password,
        environment,
        {
          id: 'authorizenet-inperson-example',
          name: 'In Person Test App',
          phone: '888-888-8888',
        }
      );

      console.log(result);
      setLoading(false);
      login(true);
    } catch (_error) {
      setError(_error.message);
      setLoading(false);
    }
  };

  return (
    <View style={styles.container}>
      {loading ? (
        <ActivityIndicator size="large" color={colors['color-info-500']} />
      ) : (
        <View style={styles.box}>
          <Text style={styles.heading}>Login</Text>
          <TextInput
            update={setUsername}
            value={username}
            placeholder="Enter your username"
          />
          <TextInput
            update={setPassword}
            value={password}
            placeholder="Enter your password"
          />
          <Switch label="Sandbox" update={setSandbox} value={sandbox} />
          <Switch label="Test Mode" update={setTest} value={test} />
          <Button
            title="Log in"
            style="color-info-500"
            onPress={attemptLogin}
          />
          {error && <Text style={styles.error}>{error}</Text>}
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: 240,
  },
  heading: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  error: {
    color: colors['color-danger-500'],
    padding: 10,
    textAlign: 'center',
  },
});
